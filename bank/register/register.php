<?php
include ('header.php');

?>

				<?php
				if(isset($_GET['error'])) {
					if($_GET['error'] == 1) {
				?>
					
						<button type="button"></button>
					
				<?php
					} else if($_GET['error'] == 2) {
				?>
					
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Użytkownik o podanym adresie e-mail już istnieje.
						</center>
                		</div>
              			</div>
		
				<?php
					} else if($_GET['error'] == 3) {
				?>
					<
						<div class="card mb-4 py-3 border-bottom-danger"style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Nie wypełniono wszystkich pól formularza.
                		</center>
						</div>
              			</div>


				
						  <?php
					} else if($_GET['error'] == 4) {
				?>
					<
						<div class="card mb-4 py-3 border-bottom-danger"style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Pole PESEL źle wypełnione
                		</center>
						</div>
              			</div>		



						  <?php
					} else if($_GET['error'] == 5) {
				?>
					<
						<div class="card mb-4 py-3 border-bottom-danger"style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Pole NIP źle wypełnione
                		</center>
						</div>
              			</div>		


						  <?php
					} else if($_GET['error'] == 6) {
				?>
					<
						<div class="card mb-4 py-3 border-bottom-danger"style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Pole REGON źle wypełnione
                		</center>
						</div>
              			</div>		


						  <?php
					} else if($_GET['error'] == 7) {
				?>
					<
						<div class="card mb-4 py-3 border-bottom-danger"style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Pole numer kontaktowy źle wypełnione
                		</center>
						</div>
              			</div>	

		


				
				<?php
					} else {
				?>
					
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Nieprawidłowa nazwa użytkownika i/lub hasło.
                		</center>
						</div>
              			</div>
				
				<?php
                        
                    }
				}
				if(isset($_GET['result'])) {
					if($_GET['result'] == 'ok') {
				?>
						<div class="card mb-4 py-3 border-bottom-success" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Sukces!</strong> Zarejestrowałeś się w serwisie UTPBank. Na podany adres e-mail został wysłany link aktywacyjny. Po aktywacji konta będziesz mógł się zalogować.
                		</center>
						</div>
              			</div>


				<?php
					}
				}
				if(isset($_GET['logout'])) {
				?>
				
						<div class="card mb-4 py-3 border-bottom-warning" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Informacja!</strong> Wylogowałeś/aś się z konta.
                		</center>
						</div>
              			</div>
					
						
				
				<?php
				}
				if(isset($_GET['notactive'])) {
				?>
				
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Twoje konto nie zostało aktywowane za pomocą wiadomości e-mail.
                		</center>
						</div>
              			</div>
				
			
				<?php
				}
				if(isset($_GET['blocked'])) {
				?>
				
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Twoje konto zostało zablokowane.
                		</center>
						</div>
              			</div>
									
				<?php
				}
				if(isset($_GET['email']) && isset($_GET['activate']) && !isset($_GET['action'])) {
					$email = clear($_GET['email']);
					$session = clear($_GET['activate']);
					$database = new Database();
					$where_user['USER_EMAIL']="='".$email."'";
					$user = $database->getRow("USER", "*", $where_user);
					if($user['USER_SESSION'] == $session) {
						$data = array(
							"USER_ACTIVE" => 1		
						);
				
						$database->updateRows('USER', $data, $where_user);
				?>
						
						<div class="card mb-4 py-3 border-bottom-success" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Sukces!</strong> Twoje konto zostało aktywowane. Możesz się teraz zalogować.</center>
						</div>
              			</div>
				
				<?php
					} else {
				?>
				
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Nieprawidłowy link aktywacyjny. Spróbuj jeszcze raz aktywować konto. Jeśli problem nie ustępuje, skontaktuj się z administratorem portalu.
						</center>
						</div>
              			</div>
				
				<?php
					}
				} else {
				
				?>

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
			  	<h4>UTPBank</h4>
                <h1 class="h4 text-gray-900 mb-4">Zarejestruj już teraz swoją firmę!</h1>
              </div>
              <form class="user" action="process.php?action=register" method="post">
                <input type="hidden" name="user-type" value="2"/>   <!--field for companies-->
                
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="company-name" id="company-name" placeholder="Pełna nazwa firmy" required>
                </div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="number" class="form-control form-control-user" name="company-nip" placeholder="NIP" required>
                  </div>

                  <div class="col-sm-6">
                    <input type="number" class="form-control form-control-user" name="company-regon" placeholder="REGON" required>
                  </div>


                </div>

                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="company-address" placeholder="ulica, numer domu &#10;kod pocztowy, miejscowość" required>
                </div>


                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="name-register" placeholder="Imię i nazwisko właścicela firmy" required>
                  
                </div>
				
				
					<div class="form-group">
                  <input type="number"  class="form-control form-control-user" name="pesel-register" id="pesel-register" placeholder="PESEL" required>
                </div>

                <div class="form-group">
                  <input type="number" class="form-control form-control-user" name="phone-register" placeholder="Telefon kontaktowy">
                </div>


				<div class="form-group">
                  <input type="email" class="form-control form-control-user" name="email-register" id="email-register" placeholder="Adres e-mail" required>
                </div>



                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" name="password-register" placeholder="Hasło" required>
                  </div>

                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" name="repeat-password" id="repeat-password" placeholder="Powtórz hasło" required>
                  </div>

                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">Zarejestruj się</button>

            
                      
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="../login/forgot-password.php?action=lost">Zapomniałem hasła</a>
              </div>
              <div class="text-center">
                <a class="small" href="../login/logowanie.html">Masz już konto ? Zaloguj się</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <?php
              }
                      ?>
  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

</body>

</html>
