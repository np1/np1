<?php

require_once("header.php");

?>

          <!-- Page Heading -->
          
		  
		  
		  <?php

if($lui['USER_TYPE'] == 2){

echo('<h1 class="h3 mb-4 text-gray-800">Twój profil</h1>');

// dla




?>
<!-- Dropdown Card Example -->
<div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edytuj swoje dane</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    
                  </div>
                </div>
<div class="card-body">
<form class="user" action="process.php?action=editprofile" method="post">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" name="company-name" id="company-name" placeholder="Pełna nazwa firmy" value="<?php echo $lui['USER_COMPANY_NAME']; ?>" required>
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control form-control-user" name="company-nip" placeholder="NIP" value="<?php echo $lui['USER_COMPANY_NIP']; ?>" required>
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" name="company-address" placeholder="Adres firmy" value="<?php echo $lui['USER_COMPANY_ADDRESS']; ?>" required>
                    </div>


                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" name="name" placeholder="Imię i nazwisko właścicela" value="<?php echo $lui['USER_NAME']; ?>" required>
                    </div>

                    <div class="form-group">
                      <input type="number" class="form-control form-control-user" name="phone" placeholder="Telefon kontaktowy" value="<?php echo $lui['USER_PHONE']; ?>" required>
                    </div>
                    


                    <button href="#" data-toggle="modal" data-target="#editProfile"  class="btn btn-primary btn-user btn-block">Zmień dane</button>

                  
                  </form>

</div>
</div>
              

<?php
}else{
	
// nie dla firmy
	
}

?>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    <?php

require_once("footer.php");

?>