<?php
include ('header.php');

?>


<?php
				
				if(isset($_GET['logout'])) {
				?>
				
						<div class="card mb-4 py-3 border-bottom-success" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Sukces!</strong> Właśnie wylogowałeś się z panelu klienta, zapraszamy ponownie!
                		</center>
						</div>
              			</div>
				
			




<?php
				
				}if(isset($_GET['notactive'])) {
				?>
				
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Twoje konto nie zostało aktywowane za pomocą wiadomości e-mail.
                		</center>
						</div>
              			</div>
				
			
				<?php
				}
				if(isset($_GET['blocked'])) {
				?>
				
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Twoje konto zostało zablokowane.
                		</center>
						</div>
              			</div>
						
 <?php
                   }     if(!isset($_GET['action']) || $_GET['action'] == 'passok') {
                            
                            if(isset($_GET['action']) && $_GET['action'] == 'passok') {
							
							echo'<div class="card mb-4 py-3 border-bottom-success" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Sukces!</strong> Twoje hasło zostało zmienione.
                		</center>
						</div>
              			</div>';
							
                            
                            }
							}
                        ?>
						
<?php						
if(isset($_GET['error'])) {
				?>
				
						<div class="card mb-4 py-3 border-bottom-danger" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Błąd!</strong> Sprawdź login bądź hasło i spróbuj ponownie.
                		</center>
						</div>
              			</div>	
						
<?php
}

?>											

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
				  <h4>UTPBank</h4>
                    <h1 class="h4 text-gray-900 mb-4">Witaj ponownie !</h1>
                  </div>
                  <form class="user" action="process.php?action=login" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" type="text" name="email" id="login" placeholder="E-mail" required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" name="password" id="password" placeholder="Hasło" required>
                    </div>
                    
                    <button type="submit"  class="btn btn-primary btn-user btn-block">Zaloguj się</button>

                  
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="forgot-password.php?action=lost">Zapomniałeś hasła ?</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="../register/rejestracja.html">Zarejestruj się</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
