<?php
require_once('../lib/session.php');
require_once('../lib/functions.php');
require_once('../lib/database.php');

if(!empty($_REQUEST['action']))
{
	$action = clear($_REQUEST['action']);
	
	switch($action)
	{
				
		case 'login':
			$database = new Database();
			$email = clear($_POST["email"]);
			$password = clear($_POST["password"]);
			$where_user['USER_EMAIL']="='".$email."'";
			$user = $database->getRow("USER", "*", $where_user);
            if($user) {
                if($user['USER_ACTIVE'] == 0) {
                    header('Location: logowanie.html?notactive');
                    exit();
                }
			if($user['USER_ACTIVE'] == 2) {
                    header('Location: logowanie.html?blocked');
                    exit();
                }		
				
            } else {
                header('Location: logowanie.html?error');
                exit();
            }
			
			$session = new Session();
			$resp = $session->login($email, $password, false);
		break;
		
		case 'logout':
			$session = new Session();
			$session->destroy_session();
			if(isset($_GET['blocked'])) {
				header('Location: logowanie.html?blocked=1');
			} else {
				header('Location: logowanie.html?logout');
			}
		break;
		
		case 'lostpassword':
			$database = new Database();
			
			$email = clear($_POST['email']);
			
			$where_user['USER_EMAIL']="='".$email."'";
			$user = $database->getRow("USER", "*", $where_user);
			if(empty($user['USER_ID'])) {
				header('Location: forgot-password.php?action=error');
				exit();
			}
			
			$session = md5(time().'fcs'.$email);
			$data = array(		
				"USER_SESSION" => $session
			);
			$from_name = "UTPBank";
			$from_email = "no-reply@np.you2.pl";
			$subject = "Resetowanie hasła w portalu UTPBank";
			$message .= 'Dziękujemy za korzystanie z serwisu <a a href="http://www.np.you2.pl/">UTPBank</a>.<br/>
			Aby dokończyć odzyskiwanie hasła kliknij w poniższy link:<br/><br/>
			<a href="http://www.np.you2.pl/bank/login/zapomnialem-hasla.html?action=lostpw&email='.$email.'&activate='.$session.'">http://www.np.you2.pl/bank/login/zapomnialem-hasla.html?action=lostpw&email='.$email.'&activate='.$session.'</a><br/><br/>
			<i>Jeśli nie rejestrowałeś się w UTPBank, po prostu zignoruj tą wiadomość.</i><br/><br/>
			Pozdrawiamy,<br/>
			Zespół <i>UTPBank</i>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: '.$from_name.' <'.$from_email.'>' . "\r\n" .
						'Reply-To: '.$from_name.' <'.$from_email.'>' . "\r\n" .
						'X-Mailer: PHP/' . phpversion();
			mail ($email, $subject , '
				<html>
					<head>
						<title>'.$subject.'</title>
					</head>
					<body>
						<p>Witaj, <br/><br/>'.$message.'</p>
					</body>
				</html>
			', $headers);
			
			$database->updateRows('USER', $data, $where_user);
			header("Location: forgot-password.php?action=sentemail");
		break;
		
		case 'newpassword':
			$database = new Database();
			
			$email = clear($_POST['email']);
			$session = clear($_POST['activate']);
			
			$where_user['USER_EMAIL']="='".$email."'";
			$user = $database->getRow("USER", "*", $where_user);
			if(empty($user['USER_ID'])) {
				header('Location: forgot-password.php?action=error&uid');
				exit();
			}
			if($user['USER_SESSION'] != $session) {
				header('Location: forgot-password.php?action=error&session');
				exit();
			}
			
			$password = md5($_POST['password']);
			$data = array(		
				"USER_PASSWORD" => $password
			);
			
			$database->updateRows('USER', $data, $where_user);
			header("Location: logowanie.html?action=passok");
		break;
            
		
	}
}
exit();
?>