<?php
include ('header.php');
?>

 <?php
                         
                            if($_GET['action'] == 'sentemail') {
                        ?>
                        
						<div class="card mb-4 py-3 border-bottom-success" style="margin-left:280px; margin-right:280px; margin-top:10px;">
               			<div class="card-body">
						<center>
						<strong>Sukces!</strong> Wysłano wiadomość na podany adres e-mail. W celu odzyskania hasła, kliknij w link w wiadomości.
                		</center>
						</div>
              			</div>
<?php
}
if($_GET['action'] == 'lost') {
                        ?>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Zapomniałeś hasła ?</h1>
                    <p class="mb-4">Nie martw się !!. W celu zresetowania hasła podaj swój adres e-mail. Otrzymasz wiadomość z linkiem do dalszego etapu odzyskiwania konta.</p>
                  </div>
                  <form class="user" action="process.php?action=lostpassword" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" name="email" placeholder="Adres e-mail" required>
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block">Zmień hasło</button>
                   


                  </form>
				  
			  
          <hr>
                 <div class="text-center">
                    <a class="small" href="../register/rejestracja.html">Zarejestruj się</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="logowanie.html">Masz już konto? Zaloguj się!</a>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
				  <?php
}
?>
        

  <?php
                      
					  
                      if($_GET['action'] == 'lostpw' && isset($_GET['email']) && isset($_GET['activate'])) {
                  ?>
				  
				  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Nowe hasło</h1>
                    <p class="mb-4">Aby dokończyć odzyskiwanie konta wpisz nowe hasło poniżej.</p>
                  </div>
                  <form class="user" action="process.php?action=newpassword" method="post">
                    <div class="form-group">
                      <input class="form-control form-control-user" type="password" name="password" placeholder="Hasło" required>
					  <input type="hidden" name="email" value="<?php echo clear($_GET['email']); ?>">
                      <input type="hidden" name="activate" value="<?php echo clear($_GET['activate']); ?>">
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block">Zmień hasło</button>
				  
				  
                 </form> 
                  
                  <div class="text-center">
                    <a class="small" href="../register/register.php">Zarejestruj się</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="formlogin.php">Masz już konto? Zaloguj się!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

                 
                  <?php
                      }
                  
                  ?>



  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
