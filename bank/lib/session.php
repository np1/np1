<?php 
session_start();
require_once('database.php');
global $database;
class Session
{	
	public $logged_in = false;
	public $user_info = array();
	
	function __construct()	{
		if($this->get("info"))		{
			//$this->logged_in = true;
			$this->user_info = $this->get("info");
			$this->login($this->user_info['USER_EMAIL'],$this->user_info['USER_PASSWORD'], true); 
		}
	}
	
	function login($username, $password, $autologin = false)	{
		
		$resp = array();		
		$database = new Database();
		$user_data = $database->getRow("USER", "*", array("USER_EMAIL" => "= '".$username."'"));
		if(!empty($user_data)) {
			if($autologin) {
				$pass = $password;
			} else {
				$pass = md5($password);
			}

			if($pass == $user_data["USER_PASSWORD"])	{
				if($user_data['USER_TYPE'] == 3) {
					header('Location: process.php?action=logout&blocked=1');
				} else {
					$this->logged_in = true;
					$this->set("info", $user_data);	
					$this->user_info = $this->get("info");				
					$resp["status"] = "ok";			
					
					if(!$autologin) {
				        if($user_data['USER_TYPE'] == 2) {
				            header('Location: ../dashboard.php');
                        } else {
				            header('Location: #');
                        }
					}
				}
			}
			else {			
				$this->destroy("info");
                if(!isset($_GET['action']) || $_GET['action'] != 'passok') {
                    $resp["status"] = "error";
                    $resp["msg"] = "Podany użytkownik nie istnieje";
                    header('Location: formlogin.php?error');
                }
			}
		}
		else
		{
			$this->destroy("info");
			$resp["status"] = "error";
			$resp["msg"] = "Podany użytkownik nie istnieje";
			header('Location: formlogin.php?error');
		}
		return $resp;
	}
	
	function set($name, $value)		{
		return $_SESSION[$name] = $value;
	}
	
	function get($name)		{
		if(!empty($_SESSION[$name]))		{
			$value = $_SESSION[$name];
		}
		else		{
			$value = false;
		}
		return $value;
	}
	
	function destroy($name)	{
		if($this->get($name))		{
			$this->set($name, null);
			unset($_SESSION[$name]);
		}
	}
	
	function destroy_session()	{
		session_destroy();
	}
}
?>