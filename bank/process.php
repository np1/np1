<?php
require_once('lib/session.php');
require_once('lib/functions.php');
require_once('lib/database.php');

if(!empty($_REQUEST['action']))
{
	$action = clear($_REQUEST['action']);
	
	switch($action)
	{
				
		case 'logout':
			$session = new Session();
			$session->destroy_session();
			if(isset($_GET['blocked'])) {
				header('Location: login/formlogin.html?blocked');
			} else {
				header('Location: login/formlogin.php?logout');
			}
		break;
		
          
		
		case 'editprofile':
			$session = new Session();
			if(!$session->logged_in)
			{
				header('Location: profile.php');
				exit;
			}
            $logged_user = $session->get("info");	
            $id = clear($logged_user['USER_ID']);
            $where['USER_ID']="=$id";
            $database = new Database();
            $name = strip_tags($_POST['name']);
            $phone = strip_tags($_POST['phone']);
            $type_url = 'profile.php';
            if($logged_user['USER_TYPE'] == 2) {
                $company_name = clear($_POST['company-name']);
                $company_address = clear($_POST['company-address']);
                $company_nip = clear($_POST['company-nip']);
                $type_url = 'profile.php';
            }
            
            if(!empty($_POST['password-repeat']) || !empty($_POST['password-new'])) {
                $password = strip_tags($_POST['password-new']);
                $repeat = strip_tags($_POST['password-repeat']);
                    
                if($password == $repeat) {
                    $new_hash = md5($password);
            
                    $data = array(
                        "USER_NAME" => $name,
                        "USER_PHONE" => $phone,
                        "USER_PASSWORD" => $new_hash,			
				        "USER_COMPANY_NAME" => $company_name,			
				        "USER_COMPANY_ADDRESS" => $company_address,			
				        "USER_COMPANY_NIP" => $company_nip
                    );
                } else {
                    header('Location: '.$type_url.'?perror=1');
				    exit;
                }
            } else {
                $data = array(
                    "USER_NAME" => $name,
                    "USER_PHONE" => $phone,			
				    "USER_COMPANY_NAME" => $company_name,			
				    "USER_COMPANY_ADDRESS" => $company_address,			
				    "USER_COMPANY_NIP" => $company_nip
                );
            }
            
            $database->updateRows('USER', $data, $where);
            header('Location: '.$type_url.'?success');
            exit();	
		break;
        
        
        case 'account':
        $database = new Database();
        $redir_url = 'kokpit.html';
       
    
        
        $account_user = clear($_POST['account-user']);
        $account_number = clear($_POST['account-number']);
        $account_date = clear($_POST['account-date']);
        $account_type = clear($_POST['account-type']);
        
        
        

        $data = array(
            "ACCOUNT_ID"=>null,
            "ACCOUNT_USER" => $account_user,	
            "ACCOUNT_NUMBER" => $account_number,		
            "ACCOUNT_DATE" => $account_date,
            "ACCOUNT_TYPE" => $account_type
        );
        
    
        
       
        
                   
        $database->insertRows('ACCOUNT', $data);
        
        header('Location: '.$redir_url.'?result=ok');





		
	}
}
exit();
?>