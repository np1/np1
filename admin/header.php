<?php
require "session.php";
$session = new Session();
if(!$session->logged_in)
{
	header('Location: login.php');
	exit;
}
$lui = $session->get("admin_info");
require_once("../bank/lib/database.php");	
$database = new Database();
$aid = $active_link_id;
$level_1_perms = array(6,18,7,11);
if ($lui['ADMIN_LEVEL'] == 1 && !in_array($aid, $level_1_perms)) {
    echo 'Brak uprawnień';
    exit;
}
?>

<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
  

    <title>UTPBank - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="lavish-bootstrap.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
	<!--<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Nav</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">UTPBANK</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            
            
            <?php
               
                    if($lui['ADMIN_LEVEL'] == 1) { 
            ?>
           
            <?php
                    } 
                    if($lui['ADMIN_LEVEL'] == 2) { 
            ?>
            
            <?php
                   
                }
            ?>
            <li><a href="process.php?action=logout">Wyloguj</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           

            <?php
              
                    if($lui['ADMIN_LEVEL'] == 1) { 
            ?>
           
		   
		    <?php
                    } 
                    if($lui['ADMIN_LEVEL'] == 2) { 
            ?>
            
            <li><a href="users.php"><i class="fa fa-user-secret" aria-hidden="true"></i>Lista klientów</a></li>
            <?php
                    } 
                
            ?>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">