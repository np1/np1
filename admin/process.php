<?php
require_once('session.php');
require_once('../bank/lib/functions.php');
require_once('../bank/lib/database.php');

if(!empty($_REQUEST['action']))
{
	$action = clear($_REQUEST['action']);
	
	switch($action)
	{


        case 'login':
			$database = new Database();
			$email = clear($_POST["email"]);
			$password = clear($_POST["password"]);
			
			$session = new Session();
			$resp = $session->login($email, $password);
		break;
		
		case 'logout':
			$session = new Session();
			$session->destroy_session();
			header('Location: login.php?logout');
		break;

            
		case 'edituser':
			$session = new Session();
			if(!$session->logged_in)
			{
				header('Location: login.php');
				exit;
			}
			if(isset($_GET['id'])) {
				$id = $_GET['id'];
				$where['USER_ID']="=$id";
                $database = new Database();
            
                $name = strip_tags($_POST['name']);
                $phone = strip_tags($_POST['phone']);
                $email = strip_tags($_POST['email']);
                $active = strip_tags($_POST['active']);
                $company_name = strip_tags($_POST['company_name']);
                $company_address = strip_tags($_POST['company_address']);
                $company_nip = strip_tags($_POST['company_nip']);
            
                $data = array(
                    "USER_NAME" => $name,
                    "USER_EMAIL" => $email,
                    "USER_PHONE" => $phone,
                    "USER_ACTIVE" => $active,
                    "USER_COMPANY_NAME" => $company_name,
                    "USER_COMPANY_ADDRESS" => $company_address,
                    "USER_COMPANY_NIP" => $company_nip
                );
                $database->updateRows('USER', $data, $where);
            }
            if(!empty($company_name)) {
                header('Location: users.php');
            } else {
                header('Location: users.php');
            }
            exit();	
		break;
            }
		
}
exit();
?>