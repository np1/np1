<?php   
session_start();
class Session
{	
	public $logged_in = false;
	public $user_info = array();
	
	function __construct()	{
		if($this->get("admin_info"))		{
			$this->logged_in = true;
			$this->user_info = $this->get("admin_info");
		}
	}
	
	function login($username, $password)	{
		global $database;
		
		$resp = array();
		
		$user_data = $database->getRow("ADMIN", "*", array("ADMIN_LOGIN" => "= '".$username."'"));
		if(!empty($user_data)) {
			$pass = md5($password);

			if($pass == $user_data["ADMIN_PASSWORD"])	{
				$this->logged_in = true;
				$this->set("admin_info", $user_data);					
				$resp["status"] = "ok";
                if($user_data['ADMIN_LEVEL'] > 1) {
				    header('Location: index.php');
                } else {
				    header('Location: news.php');
                }
			}
			else {			
				$resp["status"] = "error";
				$resp["msg"] = "User does not exist";
				header('Location: login.php?error');
			}
		}
		else
		{
			$resp["status"] = "error";
			$resp["msg"] = "User does not exist";
			header('Location: login.php?error');
		}
		return $resp;
	}
	
	function set($name, $value)		{
		return $_SESSION[$name] = $value;
	}
	
	function get($name)		{
		if(!empty($_SESSION[$name]))		{
			$value = $_SESSION[$name];
		}
		else		{
			$value = false;
		}
		return $value;
	}
	
	function destroy($name)	{
		if($this->get($name))		{
			$this->set($name, null);
			unset($_SESSION[$name]);
		}
	}
	
	function destroy_session()	{
		session_destroy();
	}
}
?>