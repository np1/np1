<?php
require "session.php";
$session = new Session();
?>
<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">

    <title>Logowanie - UTPBank Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
	<?php
		if(isset($_GET['error'])) {
		?>
			<div class="alert alert-danger" role="alert"><b>Błąd!</b> Nieprawidłowa nazwa użytkownika i/lub hasło.</div>
		<?php
		} else if(isset($_GET['logout'])) {
		?>
			<div class="alert alert-success" role="alert"><b>Sukces!</b> Pomyślnie wylogowano z systemu.</div>
		<?php
		}
	?>
      <form class="form-signin" method="POST" id="login_form" action="process.php?action=login">
        <h2 class="form-signin-heading">Proszę się zalogować</h2>
        <label for="inputEmail" class="sr-only">Login</label>
        <input value="admin" type="text" id="inputEmail" name="email" class="form-control" placeholder="Login.." required autofocus><br>
        <label for="inputPassword" class="sr-only">Hasło</label>
        <input value="admin" type="password" id="inputPassword" name="password" class="form-control" placeholder="Hasło.." required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj się</button>
		<p>
		
		</p>
      </form>
    </div> <!-- /container -->

  </body>
</html>
</html>