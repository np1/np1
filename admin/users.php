<?php

include 'header.php';
require_once('../bank/lib/functions.php');

if(!isset($_GET['action'])) {
	$all_news = $database->getRows("USER", "*", $where_users);
?>
						<h1 class="page-header">Lista klientów</h1>

                        <div role="tabpanel" class="tab-pane" id="sent">
                            <table id="1" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th class="col-sm-3">Imię i nazwisko</th>
                                        <th class="col-sm-3">Adres E-mail</th>
                                        <th class="col-sm-3">Numer telefonu</th>
                                        <th class="col-sm-1">Konto aktywne</th>
                                        <th class="col-sm-1">Edycja</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
								<?php
									mb_internal_encoding("UTF-8");								
									foreach($all_news as $sn) {
                                        if($sn['USER_ACTIVE'] == 1) {
                                            $active = 'Tak';}
										elseif($sn['USER_ACTIVE'] == 2) {
                                            $active = 'Zablokowane';	
											
                                        } else {
                                            $active = 'Nie';
                                        }
								?>
                                    <tr>
                                        <td><?php echo $sn['USER_ID']; ?></td>
                                        <td><?php echo $sn['USER_NAME']; ?></td>
                                        <td><?php echo $sn['USER_EMAIL']; ?></td>
                                        <td><?php echo $sn['USER_PHONE']; ?></td>
                                        <td><?php echo $active; ?></td>
                                        <td>
                                            <a href="users.php?action=edit&id=<?php echo $sn['USER_ID']; ?>">
                                                <button type="button" class="btn btn-warning">
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                            </a>
                                        </td>
                                       
                                    </tr>
								<?php
									}
								?>
                                </tbody>
                            </table>

                        </div>
<?php
} else {
	$action = $_GET['action'];
	if($action == 'edit') {
		if(!isset($_GET['id'])) {
			echo 'Błąd';
			die();
		}
		$id = $_GET['id'];
		$where_news['USER_ID']="=".$id."";
		$sn = $database->getRow("USER", "*", $where_news);}
?>
		<h1 class="page-header">Edytowanie klienta : <?php echo $sn['USER_NAME']; ?> </h1>
		<form action="process.php?action=edituser&id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Imię i nazwisko</label>
						<input type="text" class="form-control" name="name" value="<?php echo $sn['USER_NAME']; ?>" placeholder="Imię i nazwisko" required>
					</div>
					<div class="form-group">
						<label>Adres e-mail</label>
						<input type="e-mail" class="form-control" name="email" value="<?php echo $sn['USER_EMAIL']; ?>" placeholder="Adres e-mail" required>
					</div>
					<div class="form-group">
						<label>Numer telefonu</label>
						<input type="text" class="form-control" name="phone" value="<?php echo $sn['USER_PHONE']; ?>" placeholder="Numer telefonu" required>
					</div>
					<div class="form-group">
                        <label>Konto aktywne</label>
                            <select name="active" class="form-control">
                                <option value="1"<?php if($sn['USER_ACTIVE'] == 1) echo ' selected'; ?>>Tak</option>
                                <option value="0"<?php if($sn['USER_ACTIVE'] == 0) echo ' selected'; ?>>Nie</option>
								<option value="2"<?php if($sn['USER_ACTIVE'] == 2) echo ' selected'; ?>>Zablokowane</option>
                        </select>
					</div>
                    <?php
                        if($sn['USER_TYPE'] == 2) {
                    ?>
					<div class="form-group">
						<label>Nazwa firmy</label>
						<input type="text" class="form-control" name="company_name" value="<?php echo $sn['USER_COMPANY_NAME']; ?>" placeholder="Nazwa firmy" required>
					</div>
					<div class="form-group">
						<label for="inputDescription">Dane adresowe</label>
						<textarea class="form-control" id="inputDescription" name="company_address" rows="5"><?php echo $sn['USER_COMPANY_ADDRESS'] ?></textarea>
					</div>
					<div class="form-group">
						<label>NIP</label>
						<input type="text" class="form-control" name="company_nip" value="<?php echo $sn['USER_COMPANY_NIP']; ?>" placeholder="NIP" required>
					</div>
                    
						<button type="submit" class="btn btn-success">Zapisz</button>
		            </form>
					<?php
                        }
                    ?>
				</div>
				<div class="col-sm-6">

                <?php

                    $where_accounts['ACCOUNT_USER'] = '='.$sn['USER_ID'];
                    $accounts = $database->getRows("ACCOUNT", "*", $where_accounts);
                    if($accounts) {
                  ?>
                
                <h5>Konta klienta</h5>


                <?php
                          
                    $i = 0;
                    foreach($accounts as &$account) {
                        if($account['ACCOUNT_TYPE'] == 1) {
                                  
                            $account_type = 'Konto za 0 zł';
                        
                        } else {
                                
                            $account_type = 'Normalne konto';
                              
                        }
                              
                        $i++;
                              }
                      ?>  



                      <?php

                        if($account['ACCOUNT_STATUS'] == 1) {

                            $account_active = "Tak";


                        }else{

                            $account_active = "Nie";

                        }


                    ?>





                            <div role="tabpanel" class="tab-pane" id="sent">
                            <table id="2" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th class="col-sm-3">Data utworzenia</th>
                                        <th class="col-sm-3">Nr. konta</th>
                                        <th class="col-sm-3">Typ konta</th>
                                        <th class="col-sm-3">Aktywne</th>
                                
                                    </tr>
                                </thead>
                            <tbody>
								
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $account['ACCOUNT_DATE']; ?></td>
                                        <td><?php echo $account['ACCOUNT_NUMBER']; ?></td>
                                        <td><?php echo $account_type; ?></td>
                                        <td><?php echo $account_active; ?></td>
                                        
                                       
                                    </tr>
								
                                </tbody>
                            </table>

                        </div>
	
		</div>
		
		 <?php

				}else{

               
               
        ?>
        
       <center> Niestety, <?php echo $sn['USER_NAME']; ?> nie posiada konta w UTPBank </center>
        
        <?php        }






    
               }
          ?>
						
						
						
<?php
	

include ('footer.php');
?>
    <script>
            $('#1').DataTable({
                "language": {
                    "sProcessing": "Przetwarzanie...",
                    "sLengthMenu": "Pokaż _MENU_ pozycji",
                    "sZeroRecords": "Nie znaleziono pasujących pozycji",
                    "sInfoThousands": " ",
                    "sInfo": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                    "sInfoEmpty": "Pozycji 0 z 0 dostępnych",
                    "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                    "sInfoPostFix": "",
                    "sSearch": "Szukaj:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Pierwsza",
                        "sPrevious": "Poprzednia",
                        "sNext": "Następna",
                        "sLast": "Ostatnia"
                    },
                    "sEmptyTable": "Brak danych",
                    "sLoadingRecords": "Wczytywanie...",
                    "oAria": {
                        "sSortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                        "sSortDescending": ": aktywuj, by posortować kolumnę malejąco"
                    }
                },	
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false
                    }
				],		
				responsive: true,
				"order": [[ 0, "desc" ]]
            });
			CKEDITOR.replace( 'ckeditor', {
					extraPlugins: 'imageuploader,widgetbootstrap,youtube'
			});
    </script>